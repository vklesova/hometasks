﻿using FieldAssembly.Task1;
using System;
namespace Game
{
    class Game
    {
        Field GameField { get; set; }
        FieldPrinter GameFieldPrinter { get; set; }
        bool IsFinished { get; set; }
        string Winner { get; set; }
        bool IsFieldHasFreeCells { get; set; }
        int[,] WinnerCombination { get; set; }



        void PlayerTurn()
        {
            FreeCellsChecking();
            if (!IsFieldHasFreeCells)
            {
                Console.WriteLine("PlayerTurn(): There is no free cells");
                return;
            }
            else
            {
                Console.WriteLine("Please enter cell number:");
                ConsoleKeyInfo userInput = Console.ReadKey();
                Console.WriteLine();
                char value = userInput.KeyChar;
                bool isUserInputHasDigitValue = false;
                isUserInputHasDigitValue = IsDigit(value);
                bool isInDiaposone = false;

                if (isUserInputHasDigitValue)
                {
                    string valueC = value.ToString();
                    int digitValue = int.Parse(valueC);
                    isInDiaposone = (digitValue >= 0 && digitValue < this.GameField.Cells.Length);
                }
                while (!isInDiaposone)
                {
                    Console.WriteLine("Please enter a digit from 0 to 8");
                    userInput = Console.ReadKey();
                    Console.WriteLine();
                    value = userInput.KeyChar;
                    isUserInputHasDigitValue = IsDigit(value);
                    if (isUserInputHasDigitValue)
                    {
                        int valueDigit = int.Parse(value.ToString());
                        isInDiaposone = (valueDigit >= 0 && valueDigit < this.GameField.Cells.Length);
                    }

                }
                    string idx = value.ToString();
                    int index = int.Parse(idx);
                    this.GameField.Cells[index] = 'X';
                    Console.Clear();
                    this.GameFieldPrinter.Print(this.GameField);
                }
        }

        public bool IsDigit(char cellValue)
        {
            bool result = false;
            char[] arrayDigit = new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            for (int i = 0; i < arrayDigit.Length; i++)
            {
                if (arrayDigit[i] == cellValue)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private void FreeCellsChecking()
        {
            for (int i = 0; i < this.GameField.Cells.Length; i++)
            {
                if (IsDigit(this.GameField.Cells[i]))
                {
                    this.IsFieldHasFreeCells = true;
                    break;
                }
                else
                {
                    this.IsFieldHasFreeCells = false;
                }
            }
        }

        void CPUTurn()
            {
                FreeCellsChecking();
                if (!this.IsFieldHasFreeCells)
                {
                Console.WriteLine("CPUTurn(): There is no free cells");
                return;
            }
            else
            {
                Random random = new Random();
                int cpuChose = random.Next(0, this.GameField.Cells.Length);
                char value = this.GameField.Cells[cpuChose];
                bool isCellHasDigitValue = false;
                isCellHasDigitValue = IsDigit(value);

                while (!isCellHasDigitValue)
                {
                    cpuChose = random.Next(0, this.GameField.Cells.Length);
                    isCellHasDigitValue = IsDigit(this.GameField.Cells[cpuChose]);
                }
                this.GameField.Cells[cpuChose] = 'O';
                Console.Clear();
                this.GameFieldPrinter.Print(this.GameField);
            }
        }

        public void Start()
        {
            Initialize();
            Console.WriteLine("Game is started");
            while (!IsFinished)
            {
                PlayerTurn();
                this.Winner = GetWinner();
                this.IsFinished = !this.IsFieldHasFreeCells || this.Winner != null;
                if (this.Winner != null)
                {
                    Console.WriteLine(this.Winner + " is winner");
                    break;
                }

                CPUTurn();
                this.Winner = GetWinner();
                this.IsFinished = !this.IsFieldHasFreeCells || this.Winner != null;
                if (this.Winner != null)
                {
                    Console.WriteLine(this.Winner + " is winner");
                    break;
                }
            }
            Console.WriteLine("Game is finished");
            Console.ReadKey();
        }

        private string GetWinner()
        {
            string result = null;
            int rows = this.WinnerCombination.GetUpperBound(0) + 1;
            int columns = this.WinnerCombination.Length / rows;

            for (int i = 0; i < rows; i++)
            {
                int xCount = 0;
                int oCount = 0;
                for (int k = 0; k < columns; k++)
                {
                    int index = this.WinnerCombination[i, k];
                    char cellValue = this.GameField.Cells[index];
                    switch (cellValue)
                    {
                        case 'X': 
                            xCount++;
                            break;
                        case 'O':
                            oCount++;
                            break;
                        default:
                            break;
                    }
                }
                if (xCount==3)
                {
                    result = "Plauer";
                }
                else if (oCount==3)
                {
                    result = "CPU";
                }

            }

            return result;
        }

        private void Initialize()
        {
            this.GameField = new Field();
            this.GameField.DimensionX = 3;
            this.GameField.DimensionY = 3;
            this.GameField.Cells = new char[this.GameField.DimensionX * this.GameField.DimensionY];
            for (int i = 0; i < this.GameField.Cells.Length; i++)
            {
                char value = char.Parse(i.ToString());
                this.GameField.Cells[i] = value;
            }
            this.GameFieldPrinter = new FieldPrinter();
            this.GameFieldPrinter.Print(this.GameField);
            FillWinnerCombination();
        }

        private void FillWinnerCombination()
        {
            int[,] horizontalCombination = new int[3, 3];
            int horizontalRows = horizontalCombination.GetUpperBound(0) + 1;
            int horizontalColumns = horizontalCombination.Length / horizontalRows;
            int counter = 0;
            for (int j = 0; j < horizontalRows; j++)
            {
                for (int k = horizontalColumns - 1; k >= 0; k--)
                {
                    int index = counter + k;
                    horizontalCombination[j, k] = index;
                }
                counter += this.GameField.DimensionX;
            }

            int[,] verticalCombination = new int[3, 3];
            int verticalRows = verticalCombination.GetUpperBound(0);
            int verticalColumns = verticalCombination.Length / verticalRows;
            int counter2 = 0;
            int counter3 = 0;
            for (int j = 0; j < verticalRows + 1; j++)
            {
                for (int k = 0; k < verticalColumns - 1; k++)

                {
                    int index = counter2 + counter3;
                    verticalCombination[j, k] = index;
                    counter3 += 3;
                }
                counter2 += 1;
                counter3 = 0;
            }

            int[,] diagonalCombination = new int[2, 3];
            int diagonalRows = diagonalCombination.GetUpperBound(0) + 1;
            int diagonalColumns = diagonalCombination.Length / diagonalRows;

            for (int j = 0; j < diagonalRows; j++)
            {
                int index = 0;
                int increment = 4;
                if (j > 0)
                {
                    index = 2;
                    increment = 2;
                }

                for (int k = 0; k < diagonalColumns; k++)
                {
                    diagonalCombination[j, k] = index;
                    index += increment;
                }
            }

            this.WinnerCombination = new int[8, 3];
            Array.Copy(horizontalCombination, this.WinnerCombination, horizontalCombination.Length);
            Array.Copy(verticalCombination, 0, this.WinnerCombination, horizontalCombination.Length, verticalCombination.Length);
            Array.Copy(diagonalCombination, 0, this.WinnerCombination, horizontalCombination.Length + verticalCombination.Length, diagonalCombination.Length);
        }

    }
}
