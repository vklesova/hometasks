﻿using FieldAssembly.Task1;
using System;

namespace FieldAssembly
{
    class Program
    {
        static void Main(string[] args)
        {
            Field myField = new Field(); // экземпляр класса Field
            myField.DimensionX = 3;
            myField.DimensionY = 3;
            myField.Cells = new char [] { 'a', 'b', 'c' , 'd', 'e', 'f', 'j', 'k', 'l', 'm', 'n', 'o','p','h','i','j','k'};
            FieldPrinter myPrinter = new FieldPrinter(); // экземпляр класса FieldPrinter
            myPrinter.Print(myField);
            Console.ReadKey();
        }
    }
}
