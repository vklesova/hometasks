﻿using System;

namespace FieldAssembly.Task1
{
    public class FieldPrinter
    {
        public void Print(Field field)
        {
            for (int i = 0; i < field.DimensionY; i++)
            {
                if (i == 0)
                {
                    Console.Write("+___+");
                }
                else
                {
                    Console.Write("___+");
                }

            }
            Console.WriteLine();
            int l = 0;
            for (int i = 0; i < field.DimensionX; i++)
            {
              for (int k = 0; k < field.DimensionY; k++)
               {
                    int index = l + k;//?
                   if (k == 0)
                   {
                       
                        Console.Write("|_" + "{0}" + "_|", field.Cells[index]);
                    }
                    else
                   {
                        Console.Write("_" + "{0}" + "_|", field.Cells[index]);
                   }
                }
                Console.WriteLine();
                l += field.DimensionX;
            }
        }
    }
}
