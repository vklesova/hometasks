﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FieldAssembly.Task1
{
    public class Field
    {
        public int DimensionX { get; set; }
        public int DimensionY { get; set; }
        public char[] Cells { get; set; }
     }
}
